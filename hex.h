/*
Base16 Documentation:

https://tools.ietf.org/html/rfc4648
*/

#ifndef HEX_H
#define HEX_H

#include <stddef.h>

void hexEnc(const char* src,size_t len,char* dst);
size_t hexDec(const char* src,char* dst);

#endif
