#include "tcp.h"

#if defined(__unix__) || defined(__APPLE__)
#include <sys/socket.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#endif

int tcpSocket() {
  int sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

#ifdef _WIN32
  if (sock == INVALID_SOCKET && WSAGetLastError() == WSANOTINITIALISED) {
    WSADATA wsa;

    if (WSAStartup(0x0202,&wsa)) return -1;
    if (wsa.wVersion != 0x0202) return -1;
    sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  }
#endif

  return sock;
}

int tcpBind(int sock,uint16_t port) {
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  return bind(sock,(struct sockaddr*)&addr,sizeof(addr));
}

int tcpListen(int sock,int max) {
  return listen(sock,max);
}

int tcpAccept(int sock,struct sockaddr* addr) {
  int size = sizeof(struct sockaddr);

  return accept(sock,addr,&size);
}

int tcpConnect(int sock,const char* host,uint16_t port) {
  struct sockaddr_in addr;
  struct in_addr sin_addr;

  if ((sin_addr.s_addr = inet_addr(host)) == INADDR_NONE) { // invalid IP address, attempting to resolve host name
    struct hostent *he;

    if ((he = gethostbyname(host)) == 0) return -1; // failed.
    sin_addr = *(struct in_addr*)he->h_addr_list[0];
  }

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = sin_addr.s_addr;

  return connect(sock,(struct sockaddr*)&addr,sizeof(addr));
}

int tcpBlock(int sock,uint8_t mode) {
#ifdef _WIN32
  u_long arg = (mode)?0:1;
  return ioctlsocket(sock,FIONBIO,&arg);
#endif

#if defined(__unix__) || defined(__APPLE__)
  int opts = fcntl(sock,F_GETFL);
  return fcntl(sock,F_SETFL,mode?(opts ^ O_NONBLOCK):(opts | O_NONBLOCK));
#endif
}

int tcpSend(int sock, const char* src, size_t len) {
  return send(sock,src,len,0);
}

int tcpRecv(int sock, char* dst, size_t len) {
  return recv(sock,dst,len,0);
}

int tcpClose(int sock) {
#if defined(_WIN32)
  return closesocket(sock);
#endif

#if defined(__unix__) || defined(__APPLE__)
  return close(sock);
#endif
}
