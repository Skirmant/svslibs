/*
SMTP Documentation:

https://tools.ietf.org/html/rfc821 - Simple Mail Transfer Protocol
*/

#ifndef SMTP_H
#define SMTP_H

#define SMTP_DEFPORT 25

#define SMTP_MAXRPLY 512

char smtpSend(int sock,const char* src);
char smtpRecv(int sock,char* dst);

int smtpHelo(int sock,const char* host);
/*int smtpMail(s,???);
int smtpRcpt(s,???);
int smtpData(s,const char* data);
int smtpQuit(s);*/

#endif
