/*
HTTP/1.0 Documentation:

https://tools.ietf.org/html/rfc1945

HTTP/1.1 Documentation:

https://tools.ietf.org/html/rfc2616
*/

#include "http.h"

#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
#include <winsock2.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <sys/socket.h>
#endif

int httpHead(int sock,const char* dir,const char* field,struct http_msg* msg) {
 char* pB = msg->buffer;
 int status;

 // format HEAD request
 strcpy(pB,"HEAD /");
 if (dir) strcat(pB,dir);
 strcat(pB," HTTP/1.0\r\n");
 if (field) strcat(pB,field);
 strcat(pB,"\r\n\r\n");

 // send HEAD request
 if (send(sock,pB,strlen(pB),0) == -1) return -1;

 // receive header
 while(0 < recv(sock,(char*)pB++,1,0));
 *pB = '\0';

 // set header info
 msg->head = msg->buffer;
 msg->headLen = pB-msg->buffer;

 // find status code
 if (!(pB = strstr(msg->head,"HTTP/1."))) return 0;
 status = atoi(pB+strlen("HTTP/1.x"));

 // return status code
 return status;
}

int httpGet(int sock,const char* dir,const char* field,struct http_msg* msg) {
 char* pB = msg->buffer;
 int status;
 size_t len;

 // format GET request
 strcpy(pB,"GET /");
 if (dir) strcat(pB,dir);
 strcat(pB," HTTP/1.0\r\n");
 if (field) strcat(pB,field);
 strcat(pB,"\r\n\r\n");

 // send GET request
 if (send(sock,pB,strlen(pB),0) == -1) return -1;

 // receive header
 while(0 < recv(sock,pB,1,0)) {
  if (*pB++ == '\n') {
   recv(sock,pB,2,0);
   pB += 2;
   if ((*(pB-2) == '\r') && (*(pB-1) == '\n')) break;
  }
 }
 *pB = '\0';

 // set header info
 msg->head = msg->buffer;
 msg->headLen = pB-msg->buffer;

 // find status code
 if (!(pB = strstr(msg->head,"HTTP/1."))) return 0;
 status = atoi(pB+strlen("HTTP/1.x"));

 // find content length
if ((pB = strstr(msg->head,"Content-Length:"))) len = atoi(pB+strlen("Content-Length:"));

 // set content info
 msg->body = strchr(msg->buffer,'\0')+1;

 // receive content
 pB = msg->body;
 if (len) {
  while(len--) if (recv(sock,pB++,1,0) == -1) break;
 }
 else while(1) if (recv(sock,pB++,1,0) == -1) break;

 msg->bodyLen = pB-(msg->body);

 // return status code
 return status;
}
