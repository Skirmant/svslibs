/*
SOCKS4 Documentation:

http://www.openssh.org/txt/socks4.protocol

SOCKS4A Documentation:

http://www.openssh.com/txt/socks4a.protocol

SOCKS5 Documentation:

https://tools.ietf.org/html/rfc1928
*/

#include "socks.h"

#include <string.h>

#ifdef _WIN32
#include <winsock2.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <sys/socket.h>
#include <netdb.h>
#endif

int socks4Request(int sock, const char* host, uint16_t port, enum socks_cmd cmd) {
  char buffer[9];

  buffer[0] = 0x04; // version
  buffer[1] = cmd;  // command
  *(uint16_t*)&buffer[2] = htons(port);

  if ((*(uint32_t*)&buffer[4] = (uint32_t)inet_addr(host)) == INADDR_NONE) { // invalid IP address, attempting to resolve host name
    struct hostent *he;

    if (!(he = gethostbyname(host))) return 0; // failed.
    *(uint32_t*)&buffer[4] = *(uint32_t*)he->h_addr_list[0];
  }

  buffer[8] = '\0';

  if (send(sock,buffer,9,0) == -1) return -1;
  if (recv(sock,buffer,8,0) == -1) return -1;

  // return status code
  return buffer[1];
}

int socks4aRequest(int sock, const char* host, uint16_t port, enum socks_cmd cmd) {
  char buffer[64];
  char* pB;

  buffer[0] = 0x04; // version
  buffer[1] = cmd;  // command
  *(uint16_t*)&buffer[2] = htons(port);

  if ((*(uint32_t*)&buffer[4] = inet_addr(host)) == INADDR_NONE) { // invalid IP address, send host name
    *(uint32_t*)&buffer[4] = 0;
    buffer[7] = 255;
    buffer[8] = '\0';
    strcpy(&buffer[9],host);
    if (send(sock,buffer,9+strlen(host)+1,0) == -1) return -1;
  }
  else {
    buffer[8] = '\0';
    if (send(sock,buffer,9,0) == -1) return -1;
  }

  if (recv(sock,buffer,8,0) == -1) return -1;

  // return status code
  return buffer[1];
}
