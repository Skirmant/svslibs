#include "udp.h"

int udpSocket() {
  int sock = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);

#ifdef _WIN32
  if (sock == INVALID_SOCKET && WSAGetLastError() == WSANOTINITIALISED) {
    WSADATA wsa;

    if (WSAStartup(0x0202,&wsa)) return -1;
    if (wsa.wVersion != 0x0202) return -1;
    sock = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
  }
#endif

  return sock;
}

int udpBind(int sock,uint16_t port) {
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  return (bind(sock,(struct sockaddr*)&addr,sizeof(struct sockaddr)));

  return 1;
}

int udpTarget(struct sockaddr* addr,const char* host,uint16_t port) {
  struct in_addr sin_addr;

  if ((sin_addr.s_addr = inet_addr(host)) == INADDR_NONE) { // invalid IP address, attempting to resolve host name
    struct hostent *he;

    if (!(he = gethostbyname(host))) return -1; // failed.
    sin_addr = *(struct in_addr*)he->h_addr_list[0];
  }

  ((struct sockaddr_in*)addr)->sin_family = AF_INET;
  ((struct sockaddr_in*)addr)->sin_port = htons(port);
  ((struct sockaddr_in*)addr)->sin_addr.s_addr = sin_addr;

  return 1;
}


int udpSend(int sock,struct sockaddr* addr,const char* src,size_t len) {
  return sendto(sock,src,len,0,addr,sizeof(struct sockaddr));
}

int udpRecv(int sock,struct sockaddr* addr,char* dst,size_t len) {
  int addr_len = sizeof(struct sockaddr);

  return recvfrom(sock,dst,len,0,addr,&addr_len);
}

int udpClose(int sock) {
#if defined(_WIN32)
  return closesocket(sock);
#endif

#if defined(__unix__) || defined(__APPLE__)
  return close(sock);
#endif
}
