/*
HTTP/1.0 Documentation:

https://tools.ietf.org/html/rfc1945

HTTP/1.1 Documentation:

https://tools.ietf.org/html/rfc2616
*/

#ifndef HTTP_H
#define HTTP_H

#include <stdint.h>
#include <stddef.h>

#define HTTP_DEFPORT 80

#define HTTP_OK 200

// message structure

struct http_msg {
  char* buffer;
  char* head;
  size_t headLen;
  char* body;
  size_t bodyLen;
};

int httpHead(int sock,const char* dir,const char* field,struct httpmsg* msg);
int httpGet(int sock,const char* dir,const char* field,struct httpmsg* msg);

#endif
