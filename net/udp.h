#ifndef UDP_H
#define UDP_H

#include <stdint.h>
#include <stddef.h>

#ifdef _WIN32
#include <winsock2.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <sys/types.h>
#include <sys/socket.h> // socket(), bind(), listen(), accept(), connect(), send(), recv()
#include <unistd.h> // close()
#include <fcntl.h> // fcntl()
#endif

int udpSocket();
int udpBind(int sock,uint16_t port);
int udpTarget(struct sockaddr* addr,const char* host,uint16_t port);
int udpSend(int sock,struct sockaddr* addr,const char* src,size_t len);
int udpRecv(int sock,struct sockaddr* addr,char* dst,size_t len);
int udpClose(int sock);

#endif
