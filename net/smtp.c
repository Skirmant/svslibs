#include "smtp.h"

#include <string.h>
#include <stdlib.h>

char smtpSend(int sock,const char* src)  {
  if (send(sock,src,strlen(src),0) == -1) return -1;
  if (send(sock,"\r\n",2,0) == -1) return -1;
  else return 1;
}

char smtpRecv(int sock,char* dst) {
  while(1) {
    if (recv(sock,dst,1,0) == -1) return 0;
    else if (*dst++ == '\n') break;
  }
  *dst = '\0';

  return 1;
}

int smtpHelo(int sock,const char* host) {
  char buf[SMTP_MAXRPLY] = "\0";

  strcat(buf,"HELO ");
  strcat(buf,host);

  if (smtpSend(sock,buf) == -1) return -1;
  if (smtpRecv(sock,buf) == -1) return -1;

  return atoi(buf); // return reply code
}
