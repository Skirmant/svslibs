/*
IRC Documentation:

https://tools.ietf.org/html/rfc2810 - Internet Relay Chat: Architecture
https://tools.ietf.org/html/rfc2811 - Internet Relay Chat: Channel Management
https://tools.ietf.org/html/rfc2812 - Internet Relay Chat: Client Protocol
https://tools.ietf.org/html/rfc2813 - Internet Relay Chat: Server Protocol

http://www.irchelp.org/irchelp/rfc/rfc.html - Internet Relay Chat Protocol
*/

#include "irc.h"

#include <string.h>

#ifdef _WIN32
#include <winsock2.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <sys/socket.h>
#endif


int ircSend(int sock, struct irc_msg* msg)  {
  if (send(sock,msg->buffer,strlen(msg->buffer),0) == -1) return -1;
  if (send(sock,"\r\n",2,0) <= 0) return -1;
  else return 1;
}

int ircRecv(int sock, struct irc_msg* msg) {
  char* d = msg->buffer;

  while(1) {
    if (recv(sock,d,1,0) <= 0) return -1;
    else if (*d++ == '\n') break;
  }
  *d = '\0';

  return 1;
}

int ircParse(struct irc_msg* msg) {
  char* pB = msg->buffer;
  msg->nick = 0;
  msg->user = 0;
  msg->host = 0;
  msg->cmd = 0;
  msg->params = 0;
  msg->trail = 0;

  if (*pB++ == ':') { // standard message
    msg->nick = strtok(pB," "); // get nickname

    if (pB = strchr(msg->nick,'@')) { // get hostname
      msg->host = pB+1;
      *pB = '\0';
    }

    if (pB = strchr(msg->nick,'!')) { // get username
      msg->user = pB+1;
      *pB = '\0';
    }

    msg->cmd = strtok(NULL,"\r"); // get command

    if (pB = strstr(msg->cmd," :")) { // get trailing
      msg->trail = pB+2;
      *pB = '\0';
    }
    if (pB = strchr(msg->cmd,' ')) { // get parameters
      msg->params = pB+1;
      *pB = '\0';
    }
  }
  else if (memcmp(msg->buffer,"PING",4) == 0) {
    msg->cmd = strtok(msg->buffer," ");
    msg->params = strtok(NULL,"\r");
  }

  return 1;
}

int ircFormat(struct irc_msg* msg, enum irc_cmd cmd, const char* arg1, const char* arg2) {
  char* b = msg->buffer;
  *b = '\0';

  switch(cmd) {

  // Connection Registration

  case PASS:
    strcat(b,"PASS ");
    strcat(b,arg1);
    break;
  case NICK:
    strcat(b,"NICK ");
    strcat(b,arg1);
    break;
  case USER:
    strcat(b,"USER ");
    strcat(b,arg1);
    strcat(b," 0 * :");
    strcat(b,arg2);
    break;
  case MODE:
    strcat(b,"MODE ");
    strcat(b,arg1);
    strcat(b,arg2);
    break;
  case QUIT:
    strcat(b,"QUIT");
    if (arg1) { // optional
      strcat(b," :");
      strcat(b,arg1);
    }
    break;

  // Channel operations

  case JOIN:
    strcat(b,"JOIN ");
    strcat(b,arg1);
    break;
  case PART:
    strcat(b,"PART");
    if (arg1) {
      strcat(b," :");
      strcat(b,arg1);
    }
    break;
  case TOPIC:
    strcat(b,"TOPIC");
    if (arg1) {
      strcat(b," :");
      strcat(b,arg1);
    }
    break;
  case NAMES:
    strcat(b,"NAMES");
    if (arg1) {
      strcat(b," ");
      strcat(b,arg1);
    }
    break;

  // Sending messages

  case MSG:
    strcat(b,"PRIVMSG ");
    strcat(b,arg1);
    strcat(b," :");
    strcat(b,arg2);
    break;
  case NOTICE:
    strcat(b,"NOTICE ");
    strcat(b,arg1);
    strcat(b," :");
    strcat(b,arg2);
    break;

  // Miscellaneous messages

  case PONG:
    strcat(b,"PONG ");
    strcat(b,arg1);
    break;
  }

  strcat(b,"\r\n");

  return 1;
}
