/*
SOCKS4 Documentation:

http://www.openssh.org/txt/socks4.protocol

SOCKS4A Documentation:

http://www.openssh.com/txt/socks4a.protocol

SOCKS5 Documentation:

https://tools.ietf.org/html/rfc1928
*/

#ifndef SOCKS_H
#define SOCKS_H

#include <stdint.h>

#define SOCKS_DEFPORT 9050

#define SOCKS4_SUCCESS 90
#define SOCKS4_FAILURE 91
#define SOCKS4_RJECTED 92
#define SOCKS4_INDENTD 93

enum socks_cmd {
  CONN,
  BIND,
  UDP
}

// sends SOCKS4 request, resolves host name locally
int socks4Request(int sock, const char* host, uint16_t port, enum socks_cmd cmd);

// sends SOCKS4A request, resolves host name from the server side
int socks4aRequest(int sock, const char* host, uint16_t port, enum socks_cmd cmd);

// sends SOCKS5 request, resolves host name from the server side, supports UDP & IPv6
int socks5Request(int sock, const char* host, uint16_t port, enum socks_cmd cmd);

#endif
