#ifndef TCP_H
#define TCP_H

#include <stdint.h>
#include <stddef.h>

#ifdef _WIN32
#include <winsock2.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <netinet/in.h>
#endif

int tcpSocket();
int tcpBind(int,uint16_t);
int tcpListen(int,int);
int tcpAccept(int,struct sockaddr*);
int tcpConnect(int,const char*,uint16_t);
int tcpBlock(int,uint8_t);
int tcpSend(int,const char*,size_t);
int tcpRecv(int,char*,size_t);
int tcpClose(int);

#endif
