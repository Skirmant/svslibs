/*
IRC Documentation:

https://tools.ietf.org/html/rfc2810 - Internet Relay Chat: Architecture
https://tools.ietf.org/html/rfc2811 - Internet Relay Chat: Channel Management
https://tools.ietf.org/html/rfc2812 - Internet Relay Chat: Client Protocol
https://tools.ietf.org/html/rfc2813 - Internet Relay Chat: Server Protocol

http://www.irchelp.org/irchelp/rfc/rfc.html - Internet Relay Chat Protocol
*/

#ifndef IRC_H
#define IRC_H

#define IRC_DEFPORT 6667

#define IRC_MAXMSG 512

/// colors

#define IRC_COLOR2(x) "\3"#x
#define IRC_COLOR(x) IRC_COLOR2(x)
#define IRC_C(x) IRC_COLOR(_IRC_CC_ ## x)

#define _IRC_CC_END
#define _IRC_CC_WHITE  0
#define _IRC_CC_BLACK  1
#define _IRC_CC_DBLUE  2
#define _IRC_CC_GREEN  3
#define _IRC_CC_RED    4
#define _IRC_CC_BROWN  5
#define _IRC_CC_PURPLE 6
#define _IRC_CC_ORANGE 7
#define _IRC_CC_YELLOW 8
#define _IRC_CC_LGREEN 9
#define _IRC_CC_TEAL   10
#define _IRC_CC_LBLUE  11
#define _IRC_CC_BLUE   12
#define _IRC_CC_PINK   13
#define _IRC_CC_GRAY   14
#define _IRC_CC_LGRAY  15

// message structure

struct irc_msg {
  char buffer[IRC_MAXMSG];
  char* nick;
  char* user;
  char* host;
  char* cmd;
  char* params;
  char* trail;
};

// command enumeration

enum irc_cmd {
  PASS,
  NICK,
  USER,
  MODE,
  QUIT,
  JOIN,
  PART,
  TOPIC,
  NAMES,
  MSG,
  NOTICE,
  PONG
};

int ircSend(int sock, struct irc_msg* msg);

int ircRecv(int sock, struct irc_msg* msg);

int ircParse(struct irc_msg* msg);

int ircFormat(struct irc_msg* msg, enum irc_cmd cmd, const char* arg1, const char* arg2);

#endif
