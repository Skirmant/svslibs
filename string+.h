#ifndef STRINGPLUS_H
#define STRINGPLUS_H

#include <stdint.h>
#include <string.h>
#include <ctype.h>

size_t strcchr(const char* str,char c);
size_t strcstr(const char* str,const char* s);

void memrev(void* dst,size_t len);

void strtrim(char* str,const char* chrs);
void strcut(char* str,int start,int end);

void strlower(char* str);
void strupper(char* str);

#endif
