#include "string+.h"

// counts number of occurrences of c in the string
size_t strcchr(const char* str,char c) {
  size_t n = 0;

  while(*str) if (*str++ == c) n++;

  return n;
}

// counts number of occurrences of s in the string
size_t strcstr(const char* str,const char* s) {
  size_t len = strlen(s);
  size_t n = 0;

  while((str = strstr(str,s))) {
    str += len;
    n++;
  }

  return n;
}

// reverses memory
void memrev(void* dst,size_t len) {
  char* p1 = dst;
  char* p2 = dst+len-1;

  for(; p1 < p2; p1++,p2--) {
    *p1 ^= *p2;
    *p2 ^= *p1;
    *p1 ^= *p2;
  }
}

// removes listed characters from a string
void strtrim(char* str,const char* chrs) {
  char* pS = str;
  char* pC;

  for(; *pS; pS++) {
    for(pC = (char*)chrs; *pC; pC++) if (*pS == *pC) break;
    if (*pC == '\0') {
      *str = *pS;
      str++;
    }
  }
  *str = '\0';
}

// cuts out a part of the string
void strcut(char* str,int start,int end) {
  size_t len = strlen(str);
  char* pS = (start >= 0)?str+start:str+len+start;
  char* pE = (end >= 0)?str+end:str+len+end;

  while(*pS) *pS++ = *pE++;
}

// converts all alphabetic characters to lower case
void strlower(char* str) {
  for(; *str; str++) if (isupper(*str)) *str += 32;
}

// converts all alphabetic characters to upper case
void strupper(char* str) {
  for(; *str; str++) if (islower(*str)) *str -= 32;
}
