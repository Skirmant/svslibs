/*
JSON Documentation:

https://tools.ietf.org/html/rfc4627
http://json.org
*/

#include "json.h"

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

static char* _jsonSkipString(char* src) {
  if (*src != '"') return 0;
  for(src++; *src; src++) if ((*src == '"') && (*(src-1) != '\\')) return src+1;
  return 0;
}

static char* _jsonSkipValue(const char* src) {
  size_t btok_n = 1;
  size_t etok_n = 0;
  char token;
  char* s = (char*)src;

  // skip whitespace
  while(isspace(*s)) s++;

  token = *s;

  switch(token) { // check for token

  case '{': // skip object
    for(s++; btok_n != etok_n; s++) {
      if (*s == '{') btok_n++;
      else if (*s == '}') etok_n++;
      else if (*s == '"') if ((s = _jsonSkipString(s)) == 0) return 0;
    }
    break;

  case '[': // skip array
    for(s++; btok_n != etok_n; s++) {
      if (*s == '[') btok_n++;
      else if (*s == ']') etok_n++;
      else if (*s == '"') if ((s = _jsonSkipString(s)) == 0) return 0;
    }
    break;

  case '"': // skip string
    if ((s = _jsonSkipString(s)) == 0) return 0;
    break;

  default:
    // skip number
    if ((isdigit(*s)) || (*s ==  '-')) {
      s++;
      while((isdigit(*s)) || (*s ==  '.')) s++;
      break;
    }

    // skip true
    else if (memcmp(s,"true",4) == 0)  {
      s += 4;
      break;
    }
    // skip false
    else if (memcmp(s,"false",5) == 0)  {
      s += 5;
      break;
    }
    // skip null
    else if (memcmp(s,"null",4) == 0)  {
      s += 4;
      break;
    }

    return 0; // invalid value, syntax error
  }

  return s;
}


// finds value inside of object
char* jsonObj(const char* src, enum json_type type, char* name) {
  size_t len = strlen(name);
  char* s = (char*)src;
  char* p;

  while(isspace(*s)) s++;
  if (*s++ != '{') return 0;

  while(1) {

    // find name
    while(isspace(*s)) s++;
    if (*s++ != '"') return 0;

    // test name
    for(p = s; *s; s++) if ((*s == '"') && (*(s-1) != '\\')) break;
    if (*s == '\0') return 0;
    else if ((s-p) == len) if (strncmp(p,name,len) == 0) break; // found it!
    s++;

    // skip name separator
    while(isspace(*s)) s++;
    if (*s++ != ':') return 0;

    // skip value
    if ((s = _jsonSkipValue(s)) == 0) return 0;

    // skip value separator
    while(isspace(*s)) s++;
    if (*s++ != ',') return 0;
  }

  if (type == 0) return p-1;
  s++;

  // skip name separator
  while(isspace(*s)) s++;
  if (*s++ != ':') return 0;

  // find value
  while(isspace(*s)) s++;
  switch(type) {
  case 'o':
    if (*s == '{') return s;
    break;
  case 'a':
    if (*s == '[') return s;
    break;
  case 's':
    if (*s == '"') return s;
    break;
  case 'n':
    if ((isdigit(*s)) || (*s ==  '-')) return s;
    break;
  case 'b':
    if (memcmp(s,"true",4) == 0 || memcmp(s,"false",5) == 0) return s;
    break;
  }

  return 0;
}

// finds value inside of array
char* jsonArr(const char* src, enum json_type type, size_t index) {
  char* s = (char*)src;

  while(isspace(*s)) s++;
  if (*s++ != '[') return 0; // syntax error

  // find index
  while(index--) {

    while(isspace(*s)) s++;
    if (*s == ']') return 0; // not found
    if ((s = _jsonSkipValue(s)) == 0) return 0; // syntax error

    while(isspace(*s)) s++;
    switch(*s) {
    case ']':
      return 0;
      break;
    case ',':
      break;
    default:
      return 0;
    }

    s++;
  }

  // find value
  while(isspace(*s)) s++;
  switch(type) {
  case 'o':
    if (*s == '{') return s;
    break;
  case 'a':
    if (*s == '[') return s;
    break;
  case 's':
    if (*s == '"') return s;
    break;
  case 'n':
    if ((isdigit(*s)) || (*s ==  '-')) return s;
    break;
  case 'b':
    if (memcmp(s,"true",4) == 0 || memcmp(s,"false",5) == 0) return s;
    break;
  }

  return 0;
}

// copies the value to a string
size_t jsonCopy(const char* src, char* dst, size_t len) {
  char* s = (char*)src;

  if (*s == '"') {
    if ((s = _jsonSkipString(s)) == 0) return 0;
    s--;
  }
  else if ((s = _jsonSkipValue(s)) == 0) return 0;

  if (s-src < len) len = s-src;
  memcpy(dst,src,len);
  dst[len] = '\0';

  return len;
}

// removes value
void jsonRemove(char* str) {
  char* s = str;

  if ((s = _jsonSkipValue(s)) == 0) return;
  while(isspace(*s)) s++;
  if (*s == ':') {
    s++;
    if ((s = _jsonSkipValue(s)) == 0) return;
    while(isspace(*s)) s++;
  }
  if (*s == ',') s++;

  while(*str) *str++ = *s++;
}
