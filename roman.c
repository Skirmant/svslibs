#include "roman.h"

int romanChrValue(char c) {

  switch(c) {
    case 'I':
    return 1;
    case 'V':
    return 5;
    case 'X':
    return 10;
    case 'L':
    return 50;
    case 'C':
    return 100;
    case 'D':
    return 500;
    case 'M':
    return 1000;
  }

  return 0;
}

int romanStrValue(char* str) {
  int total = 0;
  int last = 0;

  for(; *str; str++) {
    int n = romanChrValue(*str);

    // check if symbol is invalid
    if (n == 0) return 0;

    if (last != 0 && n > last) total += n-(last*2);
    else total += n;

    last = n;
  }

  return total;
}
