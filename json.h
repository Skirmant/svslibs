/*
JSON Documentation:

https://tools.ietf.org/html/rfc4627
http://json.org
*/

#ifndef JSON_H
#define JSON_H

#include <stddef.h>

enum json_type {
  OBJ = 'o',
  ARR = 'a',
  STR = 's',
  NUM = 'n',
  BOOL= 'b'
};

char* jsonObj(const char* src,enum json_type type,char* name);
char* jsonArr(const char* src,enum json_type type,size_t index);
size_t jsonCopy(const char* src,char* dst,size_t len);

#endif
