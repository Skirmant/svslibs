#ifndef BITS_H
#define BITS_H

#include <stddef.h>
#include <stdint.h>

size_t bitGet(size_t data,uint8_t pos);
size_t bitSet(size_t data,uint8_t pos,uint8_t value);

#endif
