#include "bits.h"

size_t bitGet(size_t data,uint8_t pos) {
 return ((data >> pos) & 1);
}

size_t bitSet(size_t data,uint8_t pos,uint8_t value) {
 if (value) data |= (1 << pos);
 else data &= ~(1 << pos);

 return data;
}

/*
Byte bitsGet(Byte byte,Byte pos,Byte len) {
 Byte value = 0;
 Byte n;

 for(n = 0; n != len; n++) value = bitSet(value,n,bitGet(byte,pos+n));

 return value;
}

Byte bitsSet(Byte byte,Byte pos,Byte value,Byte len) {
 Byte n;

 for(n = 0; n != len; n++) byte = bitSet(byte,pos+n,bitGet(value,n));

 return byte;
}

char* binary(char* dst,Byte* src,size_t len) {
 Byte n;

 while(len--) {
  n = 7;
  do {
   *dst++ = '0'+bitGet(*src,n);
  } while(n--);
  *dst++ = ' ';
  src++;
 }
 *dst = '\0';

 return dst;
}*/
