/*
Base16 Documentation:

https://tools.ietf.org/html/rfc4648
*/

#include "hex.h"

#include <stdint.h>
#include <ctype.h>

#define BASE16_TABLE "0123456789ABCDEF"

#define IS_DIGIT(c) ('0' <= c && c <= '9')
#define IS_HILET6(c) ('A' <= c && c <= 'F')
#define IS_LOLET6(c) ('a' <= c && c <= 'f')

// encodes binary data into a hexadecimal string
void hexEnc(const char* src, size_t len, char* dst) {

  for(; len; len--) {
    *dst++ = BASE16_TABLE[(*src&0xf0) >> 4];
    *dst++ = BASE16_TABLE[(*src&0x0f)];

    src++;
  }

  *dst = '\0';
}

// decodes a hexadecimal string into binary data
size_t hexDec(const char* src, char* dst) {
  uint8_t val;
  const char* src_b;

  for(src_b = src; *src; src++) {

    if (IS_DIGIT(*src)) val = *src-'0';
    else if (IS_LOLET6(*src)) val = *src-'a'+10;
    else if (IS_HILET6(*src)) val = *src-'A'+10;
    else if (isspace(*src)) continue;
    else break;

    if ((src-src_b)%2 == 0) *dst = val << 4;
    else *dst++ |= val;
  }

  return src-src_b;
}
