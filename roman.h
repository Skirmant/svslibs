#ifndef ROMAN_H
#define ROMAN_H

int romanChrValue(char c);
int romanStrValue(char* str);

#endif
